//
//  TableRowController.h
//  AppleWatch
//
//  Created by Dabao on 15-3-10.
//  Copyright (c) 2015年 Dabao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WatchKit/WatchKit.h>

@interface TableRowController : NSObject
@property (weak, nonatomic) IBOutlet WKInterfaceImage *bigImageView;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *titleLabel;

@end
