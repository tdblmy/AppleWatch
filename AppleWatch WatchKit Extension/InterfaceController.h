//
//  InterfaceController.h
//  AppleWatch WatchKit Extension
//
//  Created by Dabao on 15-3-10.
//  Copyright (c) 2015年 Dabao. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

@interface InterfaceController : WKInterfaceController

@end
