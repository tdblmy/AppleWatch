//
//  InterfaceController.m
//  AppleWatch WatchKit Extension
//
//  Created by Dabao on 15-3-10.
//  Copyright (c) 2015年 Dabao. All rights reserved.
//

#import "InterfaceController.h"
#import "TableRowController.h"

@interface InterfaceController()
@property (weak, nonatomic) IBOutlet WKInterfaceTable *myTable;

@end


@implementation InterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];

    // Configure interface objects here.
    
    NSArray *titles = @[@"sunshine",
                        @"cloudy",
                        @"rain",
                        @"snow",
                        @"wind"];
    
    [self.myTable setNumberOfRows:titles.count withRowType:@"TableRowController"];
    
    for (int i = 0; i < titles.count; i ++) {
        TableRowController *cell = [self.myTable rowControllerAtIndex:i];
        [cell.bigImageView setImageNamed:@""];
        [cell.titleLabel setText:titles[i]];
    }
    
}

- (void)table:(WKInterfaceTable *)table didSelectRowAtIndex:(NSInteger)rowIndex
{
    NSLog(@"rowIndex %d",rowIndex);
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

@end



